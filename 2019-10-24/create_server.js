const events = require("events");
const net = require("net");

const createServer = () => {
  // ... ✏️

  return {
    on: () => {},
    listen: () => {}
  };
};

module.exports = createServer;
